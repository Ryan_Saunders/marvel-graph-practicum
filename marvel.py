#!/usr/bin/env python
from json import dumps

from flask import Flask, g, Response, request

#from neo4j.v1 import GraphDatabase, basic_auth, ResultError
from neo4j.v1 import GraphDatabase, basic_auth, TRUST_ON_FIRST_USE, CypherError

app = Flask(__name__, static_url_path='/static/')
#driver = GraphDatabase.driver('bolt://localhost')
driver = GraphDatabase.driver('bolt://localhost', auth=basic_auth("appUser", "marvel7"))

def get_db():
    if not hasattr(g, 'neo4j_db'):
        g.neo4j_db = driver.session()
    return g.neo4j_db

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'neo4j_db'):
        g.neo4j_db.close()

@app.route("/")
def get_index():
    return app.send_static_file('index.html')

#def serialize_movie(movie):
#    return {
#        'id': movie['id'],
#        'title': movie['title'],
#        'summary': movie['summary'],
#        'released': movie['released'],
#        'duration': movie['duration'],
#        'rated': movie['rated'],
#        'tagline': movie['tagline']
#    }

#def serialize_cast(cast):
#    return {
#        'name': cast[0],
#        'job': cast[1],
#        'role': cast[2]
#    }

def serialize_character(character):
    return {
    'character_id': character['character_id'],
    'name': character['name'],
    'description': character['description'],
    'stories': character['stories'],
    'comics': character['comics'],
    'series': character['series'],
    'events': character['events'],
    'image_url': character['image_url']
    }


@app.route("/graph")
def get_graph():
    db = get_db()
    results = db.run("MATCH (m:Movie)<-[:ACTED_IN]-(a:Person) "
             "RETURN m.title as movie, collect(a.name) as cast "
             "LIMIT {limit}", {"limit": request.args.get("limit", 100)})
    nodes = []
    rels = []
    i = 0
    for record in results:
        nodes.append({"title": record["movie"], "label": "movie"})
        target = i
        i += 1
        for name in record['cast']:
            actor = {"title": name, "label": "actor"}
            try:
                source = nodes.index(actor)
            except ValueError:
                nodes.append(actor)
                source = i
                i += 1
            rels.append({"source": source, "target": target})
    return Response(dumps({"nodes": nodes, "links": rels}),
                    mimetype="application/json")


#@app.route("/search_old")
#def get_search():
#    try:
#        q = request.args["q"]
#    except KeyError:
#        return []
#    else:
#        db = get_db()
#        results = db.run("MATCH (movie:Movie) "
#                 "WHERE movie.title =~ {title} "
#                 "RETURN movie", {"title": "(?i).*" + q + ".*"}
#        )
#        return Response(dumps([serialize_movie(record['movie']) for record in results]),
#                        mimetype="application/json")

@app.route("/search")
def get_search():
    try:
        q = request.args["q"]
    except KeyError:
        return []
    else:
        db = get_db()
        results = db.run('''
        MATCH (character:Character)
WHERE character.name =~ {name}
AND character.description IS NOT NULL
RETURN "Character" as type, character.character_id as id , character.name as name, character.description as description
order by toInt(character.comics) desc
UNION
MATCH (comic:Comic)
WHERE comic.title =~ {name}
AND comic.description IS NOT NULL
and comic.description <> "None"
RETURN "Comic" as type, comic.comic_id as id , comic.title as name, comic.description as description
UNION
MATCH (series:Series)
WHERE series.title =~ {name}
AND series.description IS NOT NULL
RETURN "Series" as type, series.series_id as id , series.title as name, series.description as description
''',
                  {"name": "(?i).*" + q + ".*"}
        )

        return Response(dumps([{
            'id': record['id'],
            'name': record['name'],
            'description': record['description'],
            'type': record['type']
        } for record in results]
        ), mimetype="application/json")
        # return Response(dumps([serialize_character(record['character']) for record in results]),
        #                mimetype="application/json")


#@app.route("/movie/<title>")
#def get_movie(title):
#    db = get_db()
#    results = db.run("MATCH (movie:Movie {title:{title}}) "
#             "OPTIONAL MATCH (movie)<-[r]-(person:Person) "
#             "RETURN movie.title as title,"
#             "collect([person.name, "
#             "         head(split(lower(type(r)), '_')), r.roles]) as cast "
#             "LIMIT 1", {"title": title})
#
#    result = results.single();
#    return Response(dumps({"title": result['title'],
#                           "cast": [serialize_cast(member)
#                                    for member in result['cast']]}),
#                    mimetype="application/json")


@app.route("/character/<character_id>")
def get_character(character_id):
    db = get_db()
    results = db.run("MATCH (character:Character {character_id:{character_id}}) "
             "RETURN character.name AS name, "
             "character.image_url AS image_url, "
             "character.comics AS comics, "
             "character.series AS series, "
             "character.stories AS stories, "
             "character.events AS events "
             "LIMIT 1", {"character_id": character_id})

    results_comics = db.run(
    "MATCH (char:Character)-[:Appears_in]-(c:Comic) "
    "WHERE char.character_id = {character_id} "
    "RETURN c.title as title "
    "LIMIT 3", {"character_id": character_id}
    )
    results_series = db.run(
    "MATCH (char:Character)-[:Appears_in]-(s:Series) "
    "WHERE char.character_id = {character_id} "
    "RETURN s.title as title "
    "LIMIT 3", {"character_id": character_id}
    )

    #Convert the results to a plain dictionary for manipulation
    result = results.single();
    result_dict = {}
    for key in result.keys():
        result_dict[key] = result[key]

    result_dict["comics"] += " (" + ", ".join([x['title'] for x in results_comics]) + ")"
    result_dict["series"] += " (" + ", ".join([x['title'] for x in results_series]) + ")"

    #print(result_dict)
    #print(result["comics"] + "(" + " ".join([x['title'] for x in results_comics]) + ")")
    return Response(dumps({"name": result_dict['name'],
                           "image_url": result_dict['image_url'],
                           "comics": result_dict['comics'],
                           "series": result_dict['series'],
                           "stories": result_dict['stories'],
                           "events": result_dict['events']
                           }),
                    mimetype="application/json")

# TODO: Add endpoints to return details for comics and series

@app.route("/comic/<comic_id>")
def get_comic(comic_id):
# Query neo4j to get the comics image url and the number of characters that appear in it
    db = get_db()
    results = db.run(
    '''
    match (c:Comic)
    where c.comic_id = {comic_id}
    optional match (char:Character)-[:Appears_in]->(c)
    return c.title as name, c.thumbnail as image_url, count(char) as num_characters
    limit 1
    ''', {"comic_id": comic_id}
    )
    result = results.single()

    return Response(dumps({
    "image_url": result['image_url'],
    "num_characters": result['num_characters'],
    "name": result['name']
    }), mimetype="application/json")

@app.route("/series/<series_id>")
def get_series(series_id):
# Query neo4j to get the comics image url and the number of characters that appear in it
    db = get_db()
    results = db.run(
    '''
    match (s:Series)
    where s.series_id = {series_id}
    optional match (char:Character)-[:Appears_in]-> (s) <-[:Part_of]-(c:Comic)
    return s.title as name, s.thumbnail as image_url, s.endYear as endYear, count(distinct char) as num_characters, count(distinct c) as num_comics
    limit 1
    ''', {"series_id": series_id}
    )
    result = results.single()

    return Response(dumps({
    "image_url": result['image_url'],
    "num_characters": result['num_characters'],
    "endYear": result['endYear'],
    "num_comics": result['num_comics'],
    "name": result['name']
    }), mimetype="application/json")

if __name__ == '__main__':
    app.run(port=8088)
